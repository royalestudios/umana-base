const { override, fixBabelImports, addLessLoader } = require('customize-cra')

// ant variables: https://github.com/ant-design/ant-design/blob/master/components/style/themes/default.less

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true,
  }),
  addLessLoader({
   javascriptEnabled: true,
   modifyVars: {
      '@primary-color': '#00A79D',
      '@processing-color': '#00A79D',
      '@error-color': '#EA354B',
      '@white': '#fff',
      '@black': '#6C6D60',
    },
 }),
)