import React, { Component } from 'react'

import './App.scss'

import Routes from './Routes'
import { ContextProvider } from './context'

import { LocaleProvider } from 'antd'
import es_ES from 'antd/lib/locale-provider/es_ES'

import HeaderNav from './components/Header'
import Footer from './components/Footer'
import './shared/structure.scss'

import dataMenu from './api/navs/nav.json'

class App extends Component {
  render() {
    return (
      <LocaleProvider locale={es_ES} >
        <ContextProvider>
          <HeaderNav  data={dataMenu.header} />
          <Routes />
          <Footer data={dataMenu.footer} redes={dataMenu.redes} />
        </ContextProvider>
      </LocaleProvider>
    )
  }
}

export default App