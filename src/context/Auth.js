import api from '../api'
import { ability, rules } from '../ability'
import head from 'lodash/head'

export default class Auth {
  constructor(updateState) {
    this.setState = updateState
  }

  login = (email, password) => {
    return new Promise((resolve, reject) => {
      // login with api
      api.user.action.login({email, password})
      .then((response) => {
        const data = response.data || {}
        // separate token from the rest of user data
        const { token, ...user } = data
        // set session
        this.setSession(token, user, resolve)
      })
      .catch((error) => {
        if (error.response) {
          // path to error message
          const errorMessage = error.response.data.message
          reject(new Error(errorMessage))
        }
      })
    })
  }

  logout = () => {
    // empty user in context
    this.setState({user: {}})

    // remove data from local storage
    localStorage.removeItem('accessToken')
    localStorage.removeItem('uid')
  }

  // renew session when the user access the app again
  renewSession = () => {
    return new Promise(async (resolve, reject) => {
      try {
        // try to get access token and user id (uid) from local storage
        const token = localStorage.getItem('accessToken')
        const uid = localStorage.getItem('uid')

        // get user data using the uid
        const response = await api.user.action.get({id: uid})
        const user = response.data || {}
        
        // set session
        this.setSession(token, user, resolve)
      } catch (error) {
        // log out if error
        this.logout()

        // handle token expiration
        if (error.message !== 'Expired token') {
          reject(error)
        }
      }
    })
  }

  setSession = async (token, user, callback = () => {}) => {
    // set access token and user id to local storage
    localStorage.setItem('accessToken', token)
    localStorage.setItem('uid', user.id)

    // NOTE: "user" must have the following properties: {
      // id: '<user id>',
      // scopes: [<name of the scope>]
    // }

    // grab the first scope (using lodash/head) and set user permissions
    await this.setUserPermissions(head(user.scopes))

    // set the user in the context's state
    this.setState({user}, callback)
  }

  // set user permissions using @casl/ability
  setUserPermissions = (scope) => {
    return new Promise((resolve) => {
      ability.on('update', resolve)
      
      // if no scope set an empty array
      if (!scope) ability.update([])
      
      else {
        // get user rules contained in src/ability/rules/<scope name>
        const userRules = rules[scope] || []
        ability.update(userRules)
      }
    })
  }
}
