import React, { Component } from 'react'
import isEmpty from 'lodash/isEmpty'

import Auth from './Auth'
// import Auth from './Auth.Firebase'

const Context = React.createContext()

export class ContextProvider extends Component {
  constructor (props) {
    super(props)
    
    this.state = {
      auth: {
        user: {},
      },
    }

    this.auth = new Auth(this.updateState('auth'))
  }

  updateState = (key) => (state, callback = () => {}) => {
    this.setState(prevState => {
      const newState = typeof state === 'function' ? state(prevState[key]) : state
      return {
        [key]: {...prevState[key], ...newState}
      }
    }, callback)
  }

  // AUTH

  get isAuthenticated() {
    const { user } = this.state.auth
    
    // return true if accessToken is in local storage and "user" isn't empty
    // TODO: CHECK TOKEN EXPIRATION IF ANY
    return localStorage.getItem('accessToken') && !isEmpty(user)
  }

  // USE THIS FOR FIREBASE
  
  // get isAuthenticated() {
  //   const { user } = this.state.auth

  //   return isEmpty(user)
  // }
  
  render () {
    return (
      <Context.Provider value={{
        auth: {
          isAuthenticated: this.isAuthenticated, 
          ...this.state.auth,
          ...this.auth
        },
      }}>
        {this.props.children}
      </Context.Provider>
    )
  }
}

export default Context