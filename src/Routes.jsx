import React from 'react'
import { Switch, Route } from 'react-router-dom'

import LoginHandler from './app/login/LoginHandler'
import { RouteCan } from './helpers/routes'

// COMPONENTS

import HelloWorld from './app/example/HelloWorld'
import NotFound from './app/other/NotFound'
import UserPage from './app/user'
import Catalogos from './app/catalogos'
// dashbord
import Dashboard from './components/Dashboard'
// companies
import CompaniesList from './app/companies'
import CompanyAdd from './app/companies/create/index'
import singleCompany from './app/companies/single/index'

const Routes = () => {
  
  return (
    <div className="umana-container">
    
      <Switch>
        
        {/* Public routes go here */}
        <Route exact path="/" component={HelloWorld} />
        <Route exact path="/user" render={(props) => <UserPage {...props} />} />

        {/* Protect every other route with login */}
        <LoginHandler>
          <Switch>
            {/* companies */}
            <Route exact path="/empresas" component={CompaniesList} />
            <Route exact path="/empresas/crear" component={CompanyAdd} />
            <Route exact path="/empresas/single:id" component={singleCompany} />
            {/* catalogos */}
            <Route exact path="/catalogos" component={Catalogos} />
            <Route exact path="/dashboard" component={Dashboard} />
            <Route exact path="/no_scope" component={HelloWorld} />
            <RouteCan I="read" a="example" exact path="/protected" component={HelloWorld} />

            {/* OTHER EXAMPLE ROUTES */}
            {/* <RouteCan I="read" a="patient" exact path="/patients" component={PatientList} /> */}
            {/* <RouteCan I="create" a="consult" exact path="/patients/:patientId/consult" component={PatientConsultCreate} /> */}

            {/* If no path matches display the Not Found component */}
            <Route component={NotFound} />

          </Switch>
        </LoginHandler>

      </Switch>
    </div>
  )
}

export default Routes