import React, { Component } from 'react'
import PropTypes from 'prop-types'
import UserApi from '../../../api/collections/user';
import { Input, Button, Form } from '../../../components';
import { notification } from 'antd';
import Context from '../../../context'
import config from '../../../api/config'

const baseURL = config.base


export default class LoginPage extends Component {
  constructor(props) { 
    super(props)

    this.base_url = {baseURL};
    /* Initial State*/
    this.state = {
      email: 'andrea@royalestudios.com',
      password: 'royale123'
    }
  }

  static contextType = Context

  componentDidMount() {
    if(localStorage.getItem('accessToken')) {
      //console.log('PROPS....', this)
    }
  }
  /* Username and password */
  inputHandler = (event) => {
    //console.log(event.target);
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  openNotificationWithIcon = (type, title, desc) => {
    notification[type]({
      message: title,
      description:
        desc,
    });
  };

 

  login = async () => {
    this.context.auth.login(this.state.email, this.state.password)

    
    
    const response = await UserApi.action.login(this.state);
    // console.log('La Respuesta....', response.data);
    if(response.status === 200 && typeof response.data.token !== undefined ) {
      this.openNotificationWithIcon('success', 'Bienvenido', `Bienvenido de nuevo ${response.data.name}`);
      localStorage.setItem('accessToken', response.data.token);
    } else {
      this.openNotificationWithIcon('error', 'Error', `Ha ocurrido un error, intente de nuevo más tarde.`);
    }
  }
  
  /** Validate email */
  validateEmail = (email) => {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  
  render() {
    const { onLogin } = this.props
    return(
      <Form className="umana-form" onSubmit={onLogin}>
          <div className="umana-form__title">
              <h2>Ingresar con correo electrónico</h2>
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="email">Correo electrónico</label>
            <Input 
              name="email"
              placeholder="Correo"
              value={this.state.email}
              onChange={this.inputHandler}
            />
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="email">Contraseña</label>
            <Input.Password
              name="password"
              placeholder="Contraseña"
              value={this.state.password}
              onChange={this.inputHandler}
            />
          </div>
          <div className="umana-form__footer">
            <Button 
              size="large"
              type="primary"
              onClick={this.login }
              disabled={!this.validateEmail(this.state.email) || this.state.password.length < 4} 
              >
              Ingresar
            </Button>
          </div>
        </Form>
    
    )
  }
}



LoginPage.propTypes = {
  onLogin: PropTypes.func,
}

LoginPage.defaultProps = {
  onLogin: (e) => {e.preventDefault()}
}
