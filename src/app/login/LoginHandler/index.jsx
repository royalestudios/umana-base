import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Context from '../../../context'

import UserPage from '../../user'

class LoginHandler extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false
    }
  }
  
  static contextType = Context

  componentDidMount () {
    const { renewSession } = this.context.auth

    if (localStorage.getItem('accessToken')) {
      this.setState({loading: true}, async () => {
        await renewSession()
        .catch((e) => console.error(e.message))

        this.setState({loading: false})
      })
    }

    window.addEventListener('focus', async () => {
      await renewSession()
      .catch((e) => console.error(e.message))
    })
  }

  onLogin = (e) => {
    e.preventDefault()
    this.context.auth.login(e.target.email.value, e.target.password.value)
    .catch((error) => {
      console.error(error)
    })
  }

  render () {
    if (this.state.loading) {
      return <div/>
    }
    if (this.context.auth.isAuthenticated) return this.props.children
    return (
      <UserPage  
        onLogin={this.onLogin} 
        loading={this.state.loading}
      />
    )
  }
}

LoginHandler.propTypes = {
  children: PropTypes.node.isRequired
}

export default LoginHandler