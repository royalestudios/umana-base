import React, { Component } from 'react';
// import PropTypes from 'prop-types';

import { Input, Button, Form } from '../../../components';


class SignUp extends Component {
  render() {
    return (
      <Form className="umana-form">
          <div className="umana-form__header">
              <h2>Registro</h2>
          </div>
          <div className="umana-form__item medium">
              <label htmlFor="email">Nombre</label>
              <Input 
                  name="name"
                  placeholder="Nombre"
              />
          </div>
          <div className="umana-form__item medium">
              <label htmlFor="email">Correo electrónico</label>
              <Input
                  name="email"
                  placeholder="Correo electrónico"
              />
          </div>
          <div className="umana-form__item medium">
              <label htmlFor="email">Contraseña</label>
              <Input.Password
                  name="password"
                  placeholder="Contraseña"
              />
          </div>
          <div className="umana-form__item medium">
              <label htmlFor="email">Confirmar contraseña</label>
              <Input.Password
                  name="password"
                  placeholder="Confirmar contraseña"
              />
          </div>
        <div className="umana-form__footer">
          <Button 
            size="large"
            type="primary" 
            >
            Registro
          </Button>
        </div>
    </Form>
    

    )
  }
}

// SignUp.propTypes = {
//   email: PropTypes.string,
//   visble: PropTypes.bool,
//   reset: PropTypes.bool,
//   onClose: PropTypes.func,
//   handleChange: PropTypes.func,
//   onSave: PropTypes.func,
//   mode: PropTypes.string
// }

export default SignUp
