import React from 'react';
import imgLogin from '../../assets/login.png'
import { Link } from 'react-router-dom'
import './index.scss'

import LoginPage from '../login/LoginPage'
// import SignUp from './Signup'

const  UserPage = () => {
    return(
        <div className="umana-login umana-layout">
          <div className="umana-login__img">
            <img src={imgLogin} alt=""/>
          </div>
          <div className="umana-login__form">
            <div className="umana-login--form">
              <LoginPage />
              {/* <SignUp /> */}
            </div>
            <div className="umana-login--button">
              <p>¿No tienes cuenta?</p> <Link to="" className="umana-button--linea">Registrate</Link>
            </div>
          </div>
        </div>
    )
}

export default UserPage;