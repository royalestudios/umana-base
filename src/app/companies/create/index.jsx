import React, { Component } from 'react'
import api from '../../../api'
import CompanyForm from './form'
import { notification } from 'antd'
import UploadAvatar from '../../../components/UploadAvatar'
import { browserHistory } from 'react-router'

class CompanyAdd extends Component {
  constructor() {
    super()

    this.state = {
      redirect: false,
      categories: [],
      categoryChildren: [],
      category_parent: {},
      category_parent_name: null,
      onDisabled: true,
      category_children: {},
      category_children_name: null,
      entity_created: '',
      avatar: null,
      mode: 'create',
      locale: {
        country: '',
        state: '',
        city: ''
      }
    }
  }

  /*
  * Get categories
  */
  componentDidMount() {
    this.getCategories()
    this.dataAvatar()
  }

  getCategories = async () => {
    const response = await api.category.action.get({})
    if (response.status === 200) {
      this.setState({
        categories: response.data.length > 0
          ? response.data.map(cat => {
            if (!cat.parent || cat.parent === '') cat.parent = null
            return cat
          })
          : response.data
      })
    }
  }

  /*
  * Change value select
  */

  handleChange = value => {
    const response = this.state.categories.filter(cat => cat.parent === value)
    this.setState({ category_parent: value })
    this.setState({ category_parent_name: this.state.categories.filter(cat => cat.id === value)[0].name })
    this.setState({ categoryChildren: this.state.categories.filter(cat => cat.parent === value) })
    
    if (response.length > 0) {
      this.setState({ onDisabled: false })
    } else {
      this.setState({ onDisabled: true })
      this.setState({ category_children: null })
      this.setState({category_children_name: null})
    }
  }
  onChange = value => {
    this.setState({ category_children: value })
    this.setState({category_children_name: this.state.categories.filter(cat => cat.id === value)[0].name })
  }
  companyLocation = (type, value, reset = false) =>
  this.setState({
    locale: { ...this.state.locale, [type]: value }
  }, () => {
    if (reset) {
      this.setState({
        locale: { ...this.state.locale, city: '' }
      })
    }
  })

  country = value => {
     this.setState({locale: { country: value }})
  }
  /*
  * Notification
  */
  openNotificationWithIcon = (type, title, desc) => {
    notification[type]({
      message: title,
      description:
        desc,
    });
  };
 

  /*
  * go to single
  */
  redirect = data => {
    // console.log(data)
    browserHistory.push("/empresas/single:" + data.name)
  }
  /*
  * get form's data 
  */
    onSave = e => {
      e.preventDefault();
      const data = {
        name: e.target.name.value,
        description: e.target.description.value,
        employees: e.target.employees.value,
        experience: e.target.experience.value,
        website: e.target.website.value,
        category: {
          id: this.state.category_parent,
          name: this.state.category_parent_name,
          sub: {
            id: this.state.category_children,
            name: this.state.category_children_name
          }
        },
        contact: {
          email: e.target.emailContact.value,
          name: e.target.nameContact.value,
          phone: e.target.phoneContact.value,
        },
        location: {
          address: e.target.address.value,
          country: this.state.locale.country,
          city: this.state.locale.city,
          province: this.state.locale.state,
          latitude: null,
          longitude: null,
          zone: parseInt(e.target.zone.value, 10),
        }
      }
      console.log('data....', data)
      this.saveCompany(data);
    }

    dataAvatar = avatar => {
      console.log('avataaaaar', avatar)
      this.setState({ avatar : avatar })
    }

    saveCompany = async data => {
      const response = await api.company.action.add(data)
      this.dataAvatar()
      console.log('avatar', this.state.avatar)
      if (response.status === 200) {
        if (this.state.avatar) {
          let dataImage = {
            module: 'companies',
            entity_id: response.data.generated_keys[0],
            media: this.state.avatar
          }
          const resp = await api.system.action.addAvatar(dataImage)
          if (resp.status === 200) setTimeout(() => {
            console.log(this.state.avatar)
            //  window.location.reload()
          }, 1000)
        }
        this.openNotificationWithIcon('success', 'Empresa Creada', 'Tu empresa ha sido creada correctamente');
       setTimeout(() => {
         this.redirect(data);
        }, 1000)
        
      }
    }

    



  render() {
    console.log('render Avatar', this.state.avatar)
    return (
      <div className="umana-layout umana-col-2">
        <div className="umana-page-title">
          <div className="umana-title">
            <h1>Perfil de Empresa</h1>
          </div>
        </div>
        <div className="umana-page-content umana-page-companieCreate page-content-large">
          {/* Avatar */}
          <div id="section-avatar" className="umana-page-companieCreate__avatar">
            <p>Cargar Imagen de perfil</p>
            <UploadAvatar
              mode={this.state.mode}
              avatar={null}
              dataSave={this.dataAvatar} />
            <p>200 x 200 px, jpg, png</p>
          </div>
          <CompanyForm
            onSave={this.onSave}
            categories={this.state.categories}
            categoryParent={this.state.category_parent}
            handleChange={this.handleChange}
            onChange={this.onChange}
            departmentValue={this.departmentValue}
            munisValue={this.munisValue}
            onDisabled={this.state.onDisabled}
            categoryChildren={this.state.categoryChildren}
            companyLocation={this.companyLocation}
            country={this.country}
          />
        </div>
      </div>
    )
  }
}

export default CompanyAdd;