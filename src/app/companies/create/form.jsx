import React, { useState } from 'react'
import { Form, Input, Button, Select, InputNumber } from '../../../components'
import Locations from '../../../api/gt.json'

const { Option } = Select

const CompanyForm = ({
  onSave,
  categories,
  categoryChildren,
  handleChange,
  onChange,
  onDisabled,
  companyLocation, 
  country,
  departmentValue,
  munisValue
  }) => {
  
  const [munis, setMunis] = useState({})



  const locationsHandler = (event, type = null) => {
    if (type === 'department') {
      const _munis = Locations.find(o => o.department === event)
      companyLocation('state', _munis.department, true)
      setMunis(_munis)
      // departmentValue(event);
    }
    if (type === 'municipality') {
      companyLocation('city', event, false)
      // munisValue(event);
    } 
      
  }
  
  return (
    <Form className="umana-form umana-page-companieCreate__form" onSubmit={onSave}>
      <div className="umana-form__body">
        {/* content */}
        <div className="umana-form__section section-content" id="section-content">
          <div className="umana-form__title">
            <h4>Información General</h4>
          </div>
          <div className="umana-form__item large">
            <label htmlFor="name">Nombre</label>
            <Input
              name="name"
              placeholder="Nombre de la empresa"
            />
          </div>
          <div className="umana-form__item large">
            <label htmlFor="description">Descripcion</label>
            <Input.TextArea
              name="description"
              placeholder="Descripcion"
            />
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="employees">Categoría</label>
            <Select
              showSearch
              placeholder="Seleccione un categoría"
              onChange={handleChange}
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }>
              {categories ?
                categories.map(cat => 
                  (!cat.parent || !cat.parent === '') ?
                   <Option key={cat.id} value={cat.id}>{cat.name}</Option>
                  : null
                )
                : null
              }
            </Select>
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="employees">Subcategoría</label>
            <Select
              showSearch
              placeholder="Seleccione un subcategoría"
              onChange={onChange}
              disabled={onDisabled}
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }>
              {categoryChildren ?
                categoryChildren.map(cat =>
                  <Option key={cat.id} value={cat.id}>{cat.name}</Option>
                )
                : null
              }
            </Select>
          </div>

          <div className="umana-form__item medium">
            <label htmlFor="employees">Numero de empleados</label>
            <Input
              name="employees"
              placeholder="Numeros de medium"
            />
          </div>
          <div className="umana-form__item large">
            <label htmlFor="experience">Años de experiencia</label>
            <Input
              name="experience"
              placeholder="Años de experiencia"
            />
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="website">Sitio Web</label>
            <Input
              name="website"
              placeholder="www.sitioweb.com"
            />
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="phone">Telefono</label>
            <Input
              name="phone"
              placeholder="0000-0000"
            />
          </div>
          <div className="umana-form__title">
            <h4>Ubicaciones</h4>
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="country">Pais</label>
            <Select
              placeholder="País"
              onChange={country}
              showSearch>
              <Option value="Guatemala" key="gt">Guatemala</Option>
            </Select>
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="province">Departamento</label>
            <Select
              onChange={e => locationsHandler(e, 'department')}
              placeholder="Seleccione"
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              showSearch>
              {
                Locations.map((o, k) =>
                  <Option
                    key={k}
                    value={o.department}>{o.department}</Option>
                )
              }
            </Select>
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="city">Municipio</label>
            <Select
              onChange={e => locationsHandler(e, 'municipality')}
              placeholder="Seleccione"
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              showSearch>
              {
                munis.municipalities
                  ? munis.municipalities.map(o =>
                    <Option
                      key={o}
                      value={o}>{o}</Option>
                  )
                  : null
              }
            </Select>
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="zone">Zona</label>
            <InputNumber
              name="zone"
              min={1}
              max={30}
              placeholder="Zona"
            />
          </div>

          <div className="umana-form__item large">
            <label htmlFor="address">Direccioón</label>
            <Input
              name="address"
              placeholder="Dirección"
            />
          </div>
          <div className="umana-form__title">
            <h4>Información de contacto</h4>
          </div>
          <div className="umana-form__item large">
            <label htmlFor="nameContact">Nombre de Contacto</label>

            <Input
              name="nameContact"
              placeholder="Nombre de Contacto"
            />
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="emailContact">Correo Electrónico</label>

            <Input
              name="emailContact"
              placeholder="Correo Electrónico"
            />
          </div>
          <div className="umana-form__item medium">
            <label htmlFor="phoneContact">Teléfono</label>
            <Input
              name="phoneContact"
              placeholder="Teléfono"
            />
          </div>
          <div className="umana-form__footer">
            <Button
              htmlType="submit">
              Guardar
                </Button>
          </div>
        </div>
      </div>
    </Form>

  )
}

export default CompanyForm;