import React, { Component } from 'react';
import api from '../../api'
import config from '../../api/config'
import { Button } from '../../components'
import companyImg from '../../assets/company-placeholder.png'
import './index.scss';
import { Link } from 'react-router-dom'
import SearchForm from '../../components/Search';

//Context
//import Context from '../../context'
//Styles

// const { auth } = useContext(Context)

class CompaniesList extends Component {
  constructor() {
    super()

    this.state={ 
      companies:[],
      addCompany: this.addCompany
    }
  }
  /*
  *  get companies api
  */
 componentDidMount() {
  this.getCompanies()
  }

  getCompanies = async () => {
    const response = await api.company.action.get({})
    if(response.status === 200) {
      this.setState({
        companies: response.data
      })
    }
  }
 
  // addCompany=() => {
  //   this.setState({companies:[...this.state.companies, {id:1, title:'Royale'}]})
  // }
 

  render() {
    // console.log('companies', this.state.companies)
      return(
          <div className="umana-layout umana-col-2">
            <div className="umana-page-title">
              <div className="umana-title title-search">
                  <h1>Empresas</h1>
              </div>
              <SearchForm/>
            </div>
            <div className="umana-page-content page-content-color">
              <ul className="umana-grid">
                  <li className="umana-grid__card create-card">
                    <Link to="/empresas/crear">
                    <Button shape="circle" icon="plus" />
                    Crear empresa
                    </Link>
                  </li>
                  {
                    this.state.companies.map(compa => 
                      <li className="umana-grid__card" key={compa.id}>
                        <div className="umana-grid-card--avatar">
                          {
                            (compa.avatar) ? 
                              <img src={`${config.base}/avatar?filename=${compa.avatar}`} alt=""/> 
                            : <img src={companyImg} alt=""/>
                          }
                        </div>
                        <h3>{compa.name}</h3>
                        <p>{compa.description}</p>
                        <Button shape="circle" icon="arrow-right" />
                      </li>
                    )
                  }
              </ul>
            </div>
          </div>
      )

  }


}

export default CompaniesList;