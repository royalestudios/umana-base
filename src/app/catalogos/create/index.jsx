import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button, Select } from '../../../components'
import { Drawer } from 'antd';
import './index.scss'

const { Option} = Select;
/*
* Drawer's functions
*/
function onBlur() {}
function onFocus() {}
function onSearch(val) {}

let isReset = false

const CreateCategory = ({ title, visible, onClose, onSave, mode, categories, handleChange, category, reset}) => {
  isReset = reset

  const beforeSubmit = e => {
    e.preventDefault()
    const self = e.target
    onSave(e)

    setTimeout(() => {
      if (isReset) {
        self.name.value = ''
      }
    }, 500)

    setTimeout(() => {
      self.name.focus()
    }, 1000);

  }

  return (
  <Drawer
    placement="right"
    // visible={true}
    visible={visible}
    onClose={onClose}
    closable={true}
    width="50%"
    mode={mode}
  >
    <h1>{title}</h1>
    {
       (mode === "edit") ? 
       console.log('categories', category)
       :
       null
    }
  
    <Form 
      onSubmit={beforeSubmit}
     >
      <div className="umana-form__item large">
        <label htmlFor="name">Nombre de la categoria</label>
        <Input
          name="name"
          placeholder="Nombre de la categoria"
        />
      </div>
      <div className="umana-form__item large">
        <label htmlFor="Categoria padre">Categoria Padre</label>
        <Select 
          defaultValue="Seleccione un categoria" 
          name="parent" 
          onChange={handleChange}
          showSearch
          optionFilterProp="children"
          onFocus={onFocus}
          onBlur={onBlur}
          onSearch={onSearch}
          filterOption={(input, option) =>
            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
          >
          {
            categories.map(cat => 
            {
              if(!cat.parent || !cat.parent === ''){
                return <Option key={cat.id} value={cat.id}>{cat.name}</Option>
              } 
            }
          )
          }

        </Select>
      </div>
      <div className="umana-form__footer">
          <Button htmlType="submit"> Guardar </Button>
          {
            (mode === "create") ? 
            <Button> Cancelar </Button> 
            : 
            <Button> Eliminar </Button>
          }
      </div>
    </Form>
  </Drawer>
  )
}

CreateCategory.propTypes = {
  title: PropTypes.string,
  visble: PropTypes.bool,
  reset: PropTypes.bool,
  onClose: PropTypes.func,
  handleChange: PropTypes.func,
  onSave: PropTypes.func,
  mode: PropTypes.string
}

CreateCategory.defaultProps = {
  title: "Nueva categoria",
  visible: false,
  reset: false,
  onClose: function() {},
  onSave: function() {},
  handleChange: function() {},
  mode: 'create',
}

export default CreateCategory