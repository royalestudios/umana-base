import React, { Component } from 'react'
import './index.scss'
import api from '../../api'
import PropTypes from 'prop-types'
import { notification, Icon } from 'antd'
import {List, Button} from '../../components'
import CreateCategory from './create';

class Catalogos extends Component {
  constructor() {
    super()
    console.log(api);
    this.state = {
      categories: [],
      showForm: false,
      title: "Nueva categoria",
      mode: "create",
      onSave: this.onSave,
      parent: null,
      category: "",
      reset: false
    }
    
  }
  openNotificationWithIcon = (type, title, desc) => {
    notification[type]({
      message: title,
      description:
        desc,
    });
  };
  /*
  * Save data
  */
  saveCategory = async (data) => {
      const response= await api.category.action.add(JSON.stringify(data))
      console.log('saveCat', response);
      if(response.status === 200) {
        this.openNotificationWithIcon('success', 'Categoria Creada', `Categoria ${response.data.name} creada correctamente`);
        this.getData()
        this.setState({parent: null, reset: true})
      }
    }

  /*
  * get form's data
  */
  onSave = e => {
    e.preventDefault(); 
    const data = {
      "name" : e.target.name.value,
      "parent" : this.state.parent,
    }
    // console.log(data);  
    this.saveCategory(data);
  }

  /*
  * Edit Function
  */
    onEditModale = e => {
      this.setState({showForm: true}) 
      this.setState({title: "Editar categoría"})
      this.setState({mode: "edit"}) 
      this.setState({category: e})
    }
  /*
  * Save modale
  */
    onSaveModale = () => {
      this.setState({showForm: true}) 
      this.setState({title: "Nueva categoría"})
      this.setState({mode: "create"}) 
    }

  /*
  * Change value select
  */

  handleChange = value => { 
    this.setState({ parent: value })
  }

  componentDidMount() {
    this.getData()
  }
  /*
  * Get data 
  */
  getData = async () => {
      const response = await api.category.action.get({})
      if(response.status === 200 ) {
        this.setState({
          categories: response.data.length > 0
            ? response.data.map(o => {
              if(!o.parent || o.parent === '') o.parent = null
              return o
            })
            : response.data
        })
      }
  }




  render() {
    return (
      <div className="umana-layout">
        <div className="umana-page-title">
          <div className="umana-title">
              <h1>Categorías y Subcategorías</h1>
          </div>
          <Button type="primary" shape="circle" icon="plus" onClick={()=> this.onSaveModale()} />
        </div>
        <div className="umana-page-content page-content-color">
            <List
              className="umana-catalogos__table"
              header={<p>Total: <span>{this.state.categories.length} Profesiones</span></p>}
              dataSource={this.state.categories.filter(o => !o.parent)}
              renderItem={item => (
                <List.Item>
                  <div className="ant-list-items--container" style={{width: '100%'}}>
                    <div className="ant-list-items--parent">
                      {item.name} 
                      <Button type="link" value={item.id} onClick={()=> this.onEditModale(item.id)}  ><Icon type="edit" theme="filled" /></Button>
                    </div>
                    {(this.state.categories.filter(o => o.parent === item.id).length > 0) ?
                      <div className="ant-list-items--childrens">
                          <List
                            dataSource={this.state.categories.filter(o => o.parent === item.id)}
                            renderItem={item => (
                              <List.Item>
                                {item.name}
                                <Button type="link" value={item.id} onClick={()=> this.onEditModale(item.id)}  ><Icon type="edit" theme="filled" /></Button>
                              </List.Item>
                            )}
                          />
                        </div>
                      :
                      null
                    }
                   
                  </div>
                </List.Item>
              )}
            />
        </div>
        <CreateCategory
            visible={this.state.showForm}
            onClose={() => this.setState({showForm: false})}
            title={this.state.title}
            mode={this.state.mode}
            handleChange={this.handleChange}
            categories={this.state.categories}
            onSave={this.onSave}
            category={this.state.category}
            reset={this.state.reset}
        />
      </div>
    )
  }
}


Catalogos.propTypes = {
  title: PropTypes.string,
  visble: PropTypes.bool,
  onClose: PropTypes.func,
  onSave: PropTypes.func,
  mode: PropTypes.string
}

Catalogos.defaultProps = {
  title: "Nueva categoria",
  visible: false,
  onClose: function() {},
  onSave: function() {},
  mode: 'create',
}

export default Catalogos;