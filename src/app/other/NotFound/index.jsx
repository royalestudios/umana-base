import React from 'react'

import './index.scss'

const NotFound = () => {
  return (
    <div className="hm-not-found" >
      <h1>Not found</h1>
    </div>
  )
}

export default NotFound