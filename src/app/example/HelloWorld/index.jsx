import React from 'react'
import sJob from '../../../assets/search-trabajo.png'
import sTal from '../../../assets/search-talento.png'
import './index.scss'
import { Link } from 'react-router-dom'

const HelloWorld = () => {
  return (
    <div className="umana-container">
      <div className="umana-layout umana-home  umana-col-2">
          <div className="umana-home__content">
            <h1>Bienvenido <br/>a Umana</h1>
            <p>We have worked with more than 500 clients in Guatemala and around the world. These are some of the biggest brands we have joined forces with</p>
          </div>
          <div className="umana-home__options">
              <Link 
                to=""
                className="umana-card grid-card-2"
              >
                <div className="umana-card__img">
                  <img src={sJob} alt=""/>
                </div>
                <div className="umana-card__content">
                  <h2>Busco Trabajo</h2>
                  <p>We have worked with more than 500 clients in Guatemala</p>
                </div>
              </Link>
              <Link 
                to=""
                className="umana-card grid-card-2"
              >
                <div className="umana-card__img">
                <img src={sTal} alt=""/>
                </div>
                <div className="umana-card__content">
                  <h2>Busco Talento</h2>
                  <p>We have worked with more than 500 clients in Guatemala</p>
                </div>
              </Link>
              

             
          </div>
      </div>
    </div>
  )
}

export default HelloWorld;
