import { AbilityBuilder } from '@casl/ability'

const { rules, can, cannot } = AbilityBuilder.extract()

// CRUD = create, read, update, delete
can('crud', ['appointment', 'consult'])
can('read', ['notifications'])
cannot('update', ['user', 'secretary'])

export default rules