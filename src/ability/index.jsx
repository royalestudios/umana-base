import { createCanBoundTo } from '@casl/react'
import ability from './ability'

import root from './rules/root'
import admin from './rules/admin'

// -- OTHER EXAMPLES --
// import admin from './rules/admin'
// import doctor from './rules/doctor'

// rules should be based on scope names
const rules = {
  root, 
  admin, 
  // doctor, 
}
const Can = createCanBoundTo(ability)

export { Can, ability, rules }