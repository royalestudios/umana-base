import React from 'react';
import PropTypes from 'prop-types';

const Select = ({ data, onChange, disabled, type }) =>
  <select
    disabled={disabled}
    onChange={type ? (e) => onChange(e, type) : onChange}>
    {data.map(op =>
      <option
        key={op.id}
        value={op.id}>
        {op.name}
      </option>
    )}
  </select>

Select.propTypes = {
  data: PropTypes.array,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func
}

Select.defaultProps = {
  data: [],
  type: '',
  disabled: false,
  onChange: function () { }
}

export default Select;