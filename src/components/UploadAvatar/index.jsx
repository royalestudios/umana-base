import React, { Component } from 'react'
import { Avatar, message } from 'antd'
import styled from 'styled-components'

const UploaderStyled = styled.input`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 99;
  text-indent: -9999em;
  cursor: pointer;
  overflow: hidden;
  outline: none;
  opacity: 0;
`

class UploadAvatar extends Component {
  constructor() {
    super()

    this.state = {
      tmpImage: null,
      imageError: false
    }
  }
  componentDidMount() {
    this.setState({
      tmpImage: this.props.avatar,
      imageError: false
    })
  }
  encodeImageFileAsURL = e => {
    const self = this
    const file = e.target.files[0]
    let reader = new FileReader()
    const _image = document.createElement('img')
    reader.onloadend = async () => {
      _image.src = reader.result
      _image.addEventListener('load', function () {
        
        if (!file || this.width > 200 || this.height > 200) {
          message.error('La imagen no puede ser mayor a 200x200 px', 10)
          self.setState({ imageError: true })
        } else {
          self.setState({ tmpImage: reader.result })
        }
      })
      if (!file || !this.state.imageError) return
      self.props.dataSave(this.state.tmpImage)
    }
    if(!file) return;
    reader.readAsDataURL(file)
  }
  render() {
    // console.log('tmpImage', this.state.tmpImage)
    return (
      <div style={{ position: 'relative', overflow: 'hidden' }}>
        <Avatar
          size={180}
          icon="user"
          src={this.state.tmpImage} />
        <UploaderStyled
          id="fileUp"
          type="file"
          name="avatar"
          onChange={this.encodeImageFileAsURL} />
      </div>
    )
  }
}

export default UploadAvatar