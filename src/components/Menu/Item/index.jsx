import React, { Component } from 'react'
import { Menu } from 'antd'
import { Link } from 'react-router-dom'

class MenuItem extends Component {
    render() {
        return(
            <li id={this.props.id}>
                <Link to={this.props.url}>
                    {this.props.icon ? 
                        (<i> {this.props.icon } </i>) :
                        ( this.props.item)
                    }
                </Link>
            </li>
        )
    }
}

export default MenuItem;