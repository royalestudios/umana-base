import React, { Component } from 'react';
import { Menu, Icon } from 'antd';
import { Link } from 'react-router-dom'
import './index.scss'
//import MenuItem from './Item';

const { SubMenu } = Menu;

class MenuNav extends Component {
  constructor() {
    super()

    this.state = {
      current: 'entrar',
    }
  }
  handleClick = e => {
    this.setState({
      current: e.key,
    });
  };

  render() {
    return (
    <Menu 
      onClick={this.handleClick} 
      mode="horizontal" 
      id={this.props.data[0].id} 
      className={this.props.data[0].className + " umana-menu"} 
      subMenuCloseDelay={1}
      >
        {
          this.props.data[0].items.map((item) => {
            if(!item.subMenu) {
              return (
                <Menu.Item key={item.name} > 
                  <Link to={item.url}>
                    {item.icon ? 
                      (<Icon type={item.icon } /> ) :
                      ( item.name)
                    }
                  </Link>
                </Menu.Item>
              )
            } else {
              return (  
                <SubMenu
                  className="ant-menu-item"
                  theme="dark"
                  title={item.name}
                  key={item.name}
                >
                  {
                    item.subMenu.map((subItem) => {
                      return(
                        <Menu.Item key={subItem.name} > 
                          <Link to={subItem.url}>
                            {subItem.name}
                          </Link>
                        </Menu.Item>
                      )
                    })
                  }
                </SubMenu>
                  
              )
            }
          })
        }
      </Menu>
    )
  }
}

export default MenuNav