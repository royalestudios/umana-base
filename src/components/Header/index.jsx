import React from 'react';
import logo from '../../assets/logo.png';
import './index.scss';
import Menu from '../Menu'
import { Link } from 'react-router-dom'

const HeaderNav = (props) => {
    return (
      <div className="umana-header">
        <Link to="/">
          <img src={logo} alt=""/>
        </Link>
        <Menu data={props.data} />
     </div>
    )
  }

export default HeaderNav;
  