import React, { Component } from 'react'
import { Icon } from  'antd'
import { Input, Form } from '../../components'
import './index.scss'

class SearchForm extends Component {
  render() {
    return (
      <div className="umana-search">
        <Form>
          <Icon type="search" />
          <Input />
        </Form>
      </div>
    )
  }
}

export default SearchForm;