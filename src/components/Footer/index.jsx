import React from 'react'
import Menu from '../Menu'
import './index.scss'

const Footer = (props) => {
    return( 
        <div className="umana-footer">
            <div className="umana-footer__container">
                <div className="umana-footer__menu">
                    <p>© Copyright</p>
                    <Menu data={props.data} />
                </div>
                <div className="umana-footer__redes">
                    <Menu data={props.redes} />
                </div>
            </div>
        </div>
    )

}

export default Footer