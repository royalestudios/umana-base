import axios from 'axios'
import config from '../config'

const baseURL = config.base
const headers = config.getHeaders()

const system = {
  action: {
    addAvatar: async (body) => {
      return await axios.post(`${baseURL}/avatar`, JSON.stringify(body), {headers})
      .then(function (response) {
        return response;
      }).catch(function (error) {
        return error;
      });

    },
    // getCategories: async (params) => {
    //   return await axios.get(`${baseURL}/category`, {
    //     headers: {
    //       'Content-Type': 'application/json',
    //       'Authorization': _token
    //     }
    //   })
    //     .then(res => {
    //       return res
    //     })
    //     .catch(function (error) {
    //       // handle error
    //       console.log(error);
    //     })
    // },
    // get: async (params) => {
    //   const headers = config.getHeaders()
    //   return await axios.get(`${baseURL}/company`, {params, headers})
    // }
  }
}


export default system;