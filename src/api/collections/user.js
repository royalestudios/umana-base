import axios from 'axios'
import config from '../config'

const baseURL = config.base

const UserApi = {
  action: {
    login: async(body) => {
      const headers = config.getHeaders()
      // remove "Authorization" from headers, since it'll be empty if logging in
      const { Authorization, ...loginHeaders } = headers
      return await axios.post(`${baseURL}/login`, JSON.stringify(body), {headers: loginHeaders})
    },
    get: async (params) => {
      const headers = config.getHeaders()
      return await axios.get(`${baseURL}/user`, {headers, params})
    }
  },
}

export default UserApi;