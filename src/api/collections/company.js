import axios from 'axios'
import config from '../config'

const baseURL = config.base
const headers = config.getHeaders()

const company = {
  action: {
    add: async body => {
      return await axios.post(`${baseURL}/company`, JSON.stringify(body), {headers})
      .then(function (response) {
        return response;
      }).catch(function (error) {
        return error;
      });

    },
    get: async params => {
      const headers = config.getHeaders()
      return await axios.get(`${baseURL}/company`, {params, headers})
    }
  }
}


export default company;