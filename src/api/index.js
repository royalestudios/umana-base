import user from './collections/user'
import category from './collections/category'
import company from './collections/company'
import system from './collections/system'

const api = {
  user,
  category,
  company,
  system
  // NOTE: set te rest of collections here
}

export default api