let config = {
  // development: 'http://128.199.162.22:3026/api/v1',
  // production:  'http://128.199.162.22:3026/api/v1',
  development: 'http://localhost:3026/api/v1',
  production:  'http://localhost:3026/api/v1'
}

config.base = config[process.env.NODE_ENV]

config.getHeaders = () => ({
  'Content-Type': 'application/json',
  'Authorization': localStorage.getItem('accessToken')
})

export default config